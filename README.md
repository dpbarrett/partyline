## Party Line
A Decal plugin for proxying chat back and forth from a discord channel and an ingame allegiance channel. It uses a discord bot to read the contents of the discord chat, and a webhook to send ingame allegiance chat to discord. A webhook is used so that we can change the username of the message author to match ingame names.

### Setup
1. Make sure you have decal >= 2.9.8.0 (**This will currently install on older versions of decal, and then break your game.  You have been warned**)
2. Make sure you have [dotnet 4.8](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net48) installed
3. Download and run the latest installer from [releases](https://gitlab.com/trevis/partyline/-/releases)
4. Create a Discord bot, add it to your server, and get a LoginToken. [Instructions Here](https://www.writebots.com/discord-bot-token/).
5. Get your discord channel ID for the channel you want to forward to/from. [Instructions Here](https://turbofuture.com/internet/Discord-Channel-ID)
6. Create a webhook for incoming discord messages. Open Discord Server Settings -> Webhooks -> Create Webhook.

### Usage
- Create a character dedicated to forwarding discord messages (or use an existing one is fine too)
- Open the plugin window and fill in the details using the information from the setup steps above.
- Click enable.
- The bot should now be proxying messages between a discord channel and ingame allegiance chat.
- The plugin will remember your settings next run, so no need to run any login commands to enable it.

### Planned Features
- Handle @mentions, currently they dont work from ingame, and are ugly when forwarded to ingame chat.
- Handle emojis, same problem as above.
