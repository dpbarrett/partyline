﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyLine.Lib {
    internal static class Logger {
        internal static string ExceptionsLogPath { get; } = Path.Combine(PluginCore.PluginStorageDirectory, "exceptions.txt");

        internal static void LogException(Exception ex) {
            Log(ex.ToString());
        }

        internal static void Log(string m) {

            try {
                if (!Directory.Exists(Path.GetDirectoryName(ExceptionsLogPath))) {
                    Directory.CreateDirectory(Path.GetDirectoryName(ExceptionsLogPath));
                }

                using (StreamWriter writer = new StreamWriter(ExceptionsLogPath, true)) {
                    writer.WriteLine($"==== {DateTime.Now} =================");
                    writer.WriteLine(m);
                    writer.WriteLine("===================================================\n");
                    writer.Close();
                }

                WriteToChat(m);
            }
            catch {
            }
        }

        internal static void WriteToChat(string message) {
            try {
                CoreManager.Current.Actions.AddChatText("[PL] " + message, 5);
            }
            catch (Exception ex) { LogException(ex); }
        }
    }
}
